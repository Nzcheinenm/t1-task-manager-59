package ru.t1.dkononov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkononov.tm.event.ConsoleEvent;

public interface ICommand {

    @Nullable
    String getARGUMENT();

    @NotNull
    String getDESCRIPTION();

    @NotNull
    String getNAME();

    void handler(ConsoleEvent consoleEvent) throws Exception;

}
