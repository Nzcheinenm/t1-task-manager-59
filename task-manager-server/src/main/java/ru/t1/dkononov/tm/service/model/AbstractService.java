package ru.t1.dkononov.tm.service.model;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.dkononov.tm.api.repository.model.IRepository;
import ru.t1.dkononov.tm.api.services.IConnectionService;
import ru.t1.dkononov.tm.api.services.IService;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.IdEmptyException;
import ru.t1.dkononov.tm.exception.field.IndexIncorrectException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.model.AbstractModel;
import ru.t1.dkononov.tm.repository.model.AbstractRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends AbstractRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected IConnectionService connectionService;


    @NotNull
    @Autowired
    protected ApplicationContext context;


    @NotNull
    @Autowired
    private IRepository<M> repository;

    @Override
    @SneakyThrows
    public void clear() {
        repository.clear();
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.findById(id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public M add(@Nullable final M model) {
        if (model == null) throw new ProjectNotFoundException();
        @Nullable final M result;
        repository.add(model);
        result = findById(model.getId());
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Collection<M> add(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::add);
        return models;
    }

    @Override
    @NotNull
    @SneakyThrows
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::update);
        return models;
    }


    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) throws UserIdEmptyException {
        repository.remove(model);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findByIndex(@Nullable final Integer index)
            throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M result;
        result = repository.findById(id);
        remove(result);
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeByIndex(@Nullable final Integer index) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final M result;
        result = repository.findByIndex(index);
        repository.remove(result);
        return result;
    }

    @Override
    @SneakyThrows
    public void removeAll() throws UserIdEmptyException {
        repository.clear();
    }

}
