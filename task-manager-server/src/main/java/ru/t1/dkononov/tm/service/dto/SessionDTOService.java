package ru.t1.dkononov.tm.service.dto;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.dkononov.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.dkononov.tm.api.services.dto.ISessionDTOService;
import ru.t1.dkononov.tm.dto.model.SessionDTO;
import ru.t1.dkononov.tm.enumerated.Sort;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.IdEmptyException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.repository.dto.SessionDTORepository;
import ru.t1.dkononov.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, SessionDTORepository> implements ISessionDTOService {

    @NotNull
    @Getter
    @Autowired
    private SessionDTORepository repository;

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findById(@Nullable final String userId, @Nullable final String id)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO add(@Nullable final SessionDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        @Nullable final SessionDTO result;
        repository.add(model);
        result = repository.findById(model.getId());
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Collection<SessionDTO> add(@NotNull Collection<SessionDTO> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::add);
        return models;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO remove(@NotNull final String userId, @Nullable final SessionDTO model) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        model.setUserId(userId);
        repository.remove(model);
        return model;
    }

    @Override
    @Nullable
    public List<SessionDTO> findAll(@Nullable Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.findById(id) != null;
    }

    @Override
    @NotNull
    public List<SessionDTO> findAll(@Nullable String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }
}
