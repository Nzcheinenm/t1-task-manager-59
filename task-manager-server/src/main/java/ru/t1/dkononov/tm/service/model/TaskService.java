package ru.t1.dkononov.tm.service.model;


import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkononov.tm.api.repository.model.ITaskRepository;
import ru.t1.dkononov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.dkononov.tm.api.services.ITaskService;
import ru.t1.dkononov.tm.enumerated.Sort;
import ru.t1.dkononov.tm.enumerated.Status;
import ru.t1.dkononov.tm.exception.field.*;
import ru.t1.dkononov.tm.model.Project;
import ru.t1.dkononov.tm.model.Task;
import ru.t1.dkononov.tm.model.User;
import ru.t1.dkononov.tm.repository.model.SessionRepository;
import ru.t1.dkononov.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@Service
public final class TaskService extends AbstractUserOwnedService<Task, TaskRepository> implements ITaskService {

    @NotNull
    @Getter
    @Autowired
    private TaskRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(
            @Nullable final Sort sort
    ) {
        return repository.findAll(sort);
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.findById(userId, id) != null;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId)
            throws Exception {
        return repository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description)
            throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task;
        task = new Task();
        task.setName(name);
        task.setDescription(description);
        repository.add(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task;
        task = new Task();
        task.setName(name);
        repository.add(task);
        return task;
    }

    @Override
    @Nullable
    @Transactional
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    )
            throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task;
        task = repository.findById(userId, id);
        if (task == null) throw new TaskIdEmptyException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @Nullable
    @Transactional
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    )
            throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task;
        task = repository.findByIndex(userId, index);
        if (task == null) throw new TaskIdEmptyException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @Nullable
    @Transactional
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    )
            throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task;
        task = findById(userId, id);
        if (task == null) throw new TaskIdEmptyException();
        task.setStatus(status);
        return task;
    }

    @Override
    @Nullable
    @Transactional
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @NotNull final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final Task task;
        task = findByIndex(userId, index);
        if (task == null) throw new TaskIdEmptyException();
        task.setStatus(status);
        return task;
    }

    @Override
    @Transactional
    public void updateProjectIdById(@NotNull String userId, @Nullable String taskId, @Nullable String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final Task task;
        task = repository.findTaskIdByProjectId(userId, taskId, projectId);
        if (task == null) throw new TaskIdEmptyException();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task result;
        result = repository.findById(userId, id);
        remove(userId, result);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(userId, id);
    }

}
