package ru.t1.dkononov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.DataXmlLoadFasterXmlRequest;
import ru.t1.dkononov.tm.enumerated.Role;
import ru.t1.dkononov.tm.event.ConsoleEvent;

@Component
public final class DataXmlLoadFasterXmlListener extends AbstractDataListener {

    @NotNull
    public static final String DESCRIPTION = "Загрузить данные из xml faster файл.";

    @NotNull
    public static final String NAME = "data-load-xml-faster";

    @Override
    public @Nullable String getARGUMENT() {
        return null;
    }

    @Override
    public @NotNull String getDESCRIPTION() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getNAME() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@dataXmlLoadFasterXmlListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest(getToken());
        getDomainEndpoint().loadDataXmlFasterXml(request);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
